import 'package:flutter/material.dart';
import 'src/playing_card.dart';
import 'package:flutter/services.dart';

Future main() async {
  // debugPaintSizeEnabled = true;
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage('Playing Card Demo'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  MyHomePage(this.title); 

  @override
  Widget build(BuildContext context) {
    
    Hand hand = Hand([PlayingCard(6, 'c', 180), PlayingCard(8, 'c', 180),
     PlayingCard(11, 's', 180), PlayingCard(10, 's', 180)],
     );

    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(title),
        ),
        body: Center(child: DisplayHand(hand.sortedCards
          
          )));
  }
}

class Hand {
  List<PlayingCard> cards;
  List<PlayingCard> sortedCards = [];
  bool rankOrderHighToLow;
  List<String> suitOrder;
  Hand(this.cards, {this.rankOrderHighToLow=true, this.suitOrder=const ['c', 'd', 'h', 's']}) {
    suitOrder.forEach((item) {
      sortedCards = sortedCards +  sortSuit(filterHand(item));
    }
    );
  }

  List<PlayingCard> filterHand(String suit) {
    List<PlayingCard> result = cards.where((item) => item.suit==suit).toList();
    return result;
  }

  List<PlayingCard> sortSuit(List<PlayingCard> l) {
    l.sort((a,b) => a.rank>b.rank ? 1 : 0);
    if (rankOrderHighToLow) {
      return l.reversed.toList();
    }
    return l.reversed.toList(); 
}


}


class DisplayHand extends StatelessWidget {
  final List<PlayingCard> cards;
  DisplayHand(this.cards);

  Widget _positionedCard(card, index) {
    return Positioned(
      top: 0,
      bottom: index * 0.33 * cardWidth,
      child: card
    );
  }


  List<Widget> _cardStack() {
    List<Widget> temp = [];
    int index = 0;
    cards.forEach((card) {
      temp = temp.add(_positionedCard(card, index));
    });
    return temp;
  }

  @override
  Widget build(BuildContext context) {
    Widget result;
    double handHeight = cards[0].cardHeight;
    double cardWidth = cards[0].cardWidth; 
    result = Container(
      // height:handHeight,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            child: cards[0]
          ),
          Positioned(
            top: 0,
            left: 1 * 0.33 * cardWidth,
            child: cards[1]
          ),
          Positioned(
            top: 0,
            left: 2 * 0.33 * cardWidth,
            child: cards[1]
          ),
          Positioned(
            top: 0,
            left: 3 * 0.33 * cardWidth,
            child: cards[1]
          )
        ],
      )
    );
    return result;
  }
}
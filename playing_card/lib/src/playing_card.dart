import 'package:flutter/material.dart';

class PlayingCard extends StatelessWidget {
  final int rank;
  final String suit;
  final double cardWidth;
  static const double _CARD_HEIGHT = 1.39;
  static const double _10_FONT_SIZE = 0.1555;
  static const double _NON_10_FONT_SIZE = 0.175;
  static const double _COL_ONE_LETTER_SPACING_10 = -2.7;
  static const double _COL_ONE_LETTER_SPACING_NON_10 = 0;
  static const double _COL_ONE_TOP = 0.035;
  static const double _COL_OTHER_TOP = 0.12;
  static const double _COL_ONE_RANK_SUIT_HEIGHT = 0.3;
  static const double _COL_ONE_IMAGE_HEIGHT = 0.125;
  static const double _COL_OTHER_IMAGE_HEIGHT = 0.185;
  static const Map<int,String> _HONOURS = {11:'J', 12:'Q', 13:'K', 14:'A'}; 
  static const double _SUIT_HONOURS_TOP = 0.5;
  static const double _SUIT_HONOURS_LEFT = 4.5;

  final double _cardHeight;
  final double _colOneFontSize;
  final double _colOneLetterSpacing;
  final double _colOneRankSuitHeight;
  final double _colOneImageHeight;
  final double _colOneTop;
  final double _colOtherTop;
  final Widget _suitImage;
  final Widget _suitImageContainer;
  final Widget _suitImageContainerRotated;

  static Map<String, Widget> _imageLookup = {
    'c': Image.asset('images/clubs.png'),
    'd': Image.asset('images/diamonds.png'),
    'h': Image.asset('images/hearts.png'),
    's': Image.asset('images/spades.png'),
  };

  static Map<int, String> _imageLookupHonours = {
    11: 'images/jack.png',
  };



  PlayingCard(this.rank, this.suit, this.cardWidth)
      : _suitImage = _imageLookup[suit],
        _cardHeight = _CARD_HEIGHT * cardWidth,
        _colOneFontSize = rank == 10 ? _10_FONT_SIZE * cardWidth : _NON_10_FONT_SIZE * cardWidth,
        _colOneLetterSpacing = rank == 10 ? _COL_ONE_LETTER_SPACING_10 : _COL_ONE_LETTER_SPACING_NON_10,
        _colOneTop = _COL_ONE_TOP * cardWidth,
        _colOtherTop = _COL_OTHER_TOP * cardWidth,
        _colOneRankSuitHeight = _COL_ONE_RANK_SUIT_HEIGHT * cardWidth,
        _colOneImageHeight = _COL_ONE_IMAGE_HEIGHT * cardWidth,
        _suitImageContainer = Container(height: _COL_OTHER_IMAGE_HEIGHT * cardWidth, child: _imageLookup[suit]),
        _suitImageContainerRotated = Container(height: _COL_OTHER_IMAGE_HEIGHT * cardWidth,
          child: Transform.rotate(angle: 3.141, child:_imageLookup[suit]));

  double get cardHeight {return _cardHeight;}

  @override
  String toString({ DiagnosticLevel minLevel = DiagnosticLevel.debug }) {
    return 'PC-$rank $suit';
  }



  TextStyle _textStyle1() {
    return TextStyle(
        color: (suit=='c' || suit=='s') ? Colors.black : Colors.red,
        fontSize: _colOneFontSize,
        fontWeight: FontWeight.bold,
        letterSpacing: _colOneLetterSpacing);
  }

  String _cardText() {
    if (rank >= 2 && rank <= 10) {
      return rank.toString();
    }
    return _HONOURS[rank];
  }

  Widget _colOneTextAndImageContainer() {
    return Container(
            height: _colOneRankSuitHeight,
            decoration: BoxDecoration(color: Colors.transparent),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  top: 0,
                  child: Text(_cardText(), style: _textStyle1()),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    child: _suitImage,
                    height: _colOneImageHeight,
                    ),
                )
              ],
            ));
  }

  Widget _columnOne() {
    return Column(
      children: <Widget>[
        SizedBox(height: _colOneTop),
        _colOneTextAndImageContainer()
      ],
    );
  }

  Widget _columnFive() {
    return Column(
      verticalDirection: VerticalDirection.up,
      children: <Widget>[
        SizedBox(height: _colOneTop),
        RotatedBox(
          quarterTurns: 2,
          child: _colOneTextAndImageContainer()
        )
      ]);
  }

  Widget _columnTwoFour() {
    Widget result;
    if (rank == 2 || rank == 3 || rank == 14) {
      result = Column();
    }
    if (rank == 4 || rank == 5) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          _suitImageContainer,
          Expanded(flex: 100, child: Container()),
          _suitImageContainerRotated,
          SizedBox(height: _colOtherTop)
        ],
      );
    }
    if (rank == 6 || rank == 7 || rank == 8) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          _suitImageContainer,
          Expanded(flex: 20, child: Container()),
          _suitImageContainer,
          Expanded(flex: 20, child: Container()),
          _suitImageContainerRotated,
          SizedBox(height: _colOtherTop),
        ],
      );
    }
    if (rank == 9 || rank == 10) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          _suitImageContainer,
          Expanded(flex: 20, child: Container()),
          _suitImageContainer,
          Expanded(flex: 20, child: Container()),
          _suitImageContainerRotated,
          Expanded(flex: 20, child: Container()),
          _suitImageContainerRotated,
          SizedBox(height: _colOtherTop),
        ],
      );
    }
    return result;
  }

  Widget _columnThree() {
    Widget result;
    if (rank == 2) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          _suitImageContainer,
          Expanded(child: Container(), flex: 100),
          _suitImageContainerRotated,
          SizedBox(height: _colOtherTop),
        ],
      );
    }
    if (rank == 3) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          _suitImageContainer,
          Expanded(child: Container(), flex: 50),
          _suitImageContainer,
          Expanded(child: Container(), flex: 50),
          _suitImageContainer,
          SizedBox(height: _colOtherTop),
        ],
      );
    }
    if (rank == 4 || rank == 6) {result = Column();}
    if (rank == 5 || rank == 9 || rank==14) {
      result = Column(children: <Widget>[
          Expanded(child: Container(), flex: 50),
          _suitImageContainer,
          Expanded(child: Container(), flex: 50),
        ],
      );
    }
    if (rank == 7) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          Expanded(child: Container(), flex: 15),
          _suitImageContainer,
          Expanded(child: Container(), flex: 50),
        ]
      );
    }
    if (rank == 8) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          Expanded(child: Container(), flex: 14),
          _suitImageContainer,
          Expanded(child: Container(), flex: 15),
          _suitImageContainerRotated,
          Expanded(child: Container(), flex: 14),
          SizedBox(height: _colOtherTop),
        ]
      );
    }
    if (rank == 10) {
      result = Column(
        children: <Widget>[
          SizedBox(height: _colOtherTop),
          Expanded(child: Container(), flex: 18),
          _suitImageContainer,
          Expanded(child: Container(), flex: 50),
          _suitImageContainerRotated,
          Expanded(child: Container(), flex: 17),
          SizedBox(height: _colOtherTop),
        ]
      );

    }
    return result;
  }

  Widget _suitHonours() {
    return Positioned(
        top: _SUIT_HONOURS_TOP,
        left: _SUIT_HONOURS_LEFT,
        child: _suitImageContainer
    );
  }

  Widget _suitHonoursRotated() {
    return Positioned(
        bottom: _SUIT_HONOURS_TOP,
        right: _SUIT_HONOURS_LEFT,
        child: _suitImageContainerRotated
    );
  }

  Widget _honoursImage() {
    return Container(
      constraints: BoxConstraints.expand(),
      child: FittedBox(child: Image.asset(_imageLookupHonours[rank]), fit: BoxFit.fill)
    );
  }

  Widget _colHonours() {
    return Column(children: <Widget>[
      SizedBox(height: _colOtherTop,),
      Expanded(
        flex: 100,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                child: _honoursImage()
              ),
              _suitHonours()
            ]
          )
        )),
      Expanded(
        flex: 100,
        child: Container(color: Colors.grey,
          child: Stack(

            children: <Widget>[
              Positioned(
                child: RotatedBox(
                  quarterTurns: 2,
                  child: _honoursImage()
                )
              ),
              _suitHonoursRotated(),
            ]
          )
        )),
      SizedBox(height: _colOtherTop,),
    ],);
  }


  @override
  Widget build(BuildContext context) {
    Widget result;
    if ((rank>=2 && rank<=10) || rank==14) {
    result = Container(
        width: cardWidth,
        height: _cardHeight,
        color: Colors.transparent,
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
                border: Border.all(color: Colors.grey)),
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 17,
                    child: Container(
                      child: _columnOne(),
                    )),
                Expanded(
                    flex: 22,
                    child: Container(
                        child: _columnTwoFour())),
                Expanded(
                    flex: 22,
                    child: Container(
                        child: _columnThree())),
                Expanded(
                  flex: 22,
                  child: Container(
                      child: _columnTwoFour()),
                ),
                Expanded(
                    flex: 17,
                    child: Container(
                      child: _columnFive(),
                    )),
              ],
            )));
  }
    if (rank>=11 && rank<=13) {
      result = Container(
          width: cardWidth,
          height: _cardHeight,
          color: Colors.transparent,
          child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  border: Border.all(color: Colors.grey)),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 17,
                      child: Container(
                        child: _columnOne(),
                      )),
                  Expanded(
                    flex: 66,
                      child: Container(child:_colHonours())
                    ),
                Expanded(
                    flex: 17,
                    child: Container(
                      child: _columnFive(),
                    )),
                ]
              )
          )
      );
    }
    return result;
  }
}
